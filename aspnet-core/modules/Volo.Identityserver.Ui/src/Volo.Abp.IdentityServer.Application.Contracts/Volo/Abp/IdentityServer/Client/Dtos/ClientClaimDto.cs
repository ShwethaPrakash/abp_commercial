﻿using System;

namespace Volo.Abp.IdentityServer.Client.Dtos
{
    public class ClientClaimDto
    {
        public string Type { get; set; }

        public string Value { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Volo.Abp.IdentityServer.Web.Navigation
{
    public class AbpIdentityServerMenuNames
    {
        public const string GroupName = "IdentityServer";

        public const string ApiResources = GroupName + ".ApiResources";
        public const string ApiScopes = GroupName + ".ApiScopes";
        public const string Clients = GroupName + ".Clients";
        public const string IdentityResources = GroupName + ".IdentityResources";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Volo.Abp.BlazoriseUI;
using Volo.Abp.IdentityServer.ApiResource.Dtos;
using Volo.Abp.IdentityServer.ApiScope;
using Volo.Abp.IdentityServer.ClaimType;
using Volo.Abp.IdentityServer.Localization;

namespace Volo.Abp.IdentityServer.Blazor.Pages.IdentityServer
{
    public partial class ApiResourcesManagement
    {
        [Inject]
        protected IIdentityServerClaimTypeAppService ClaimTypeAppService { get; set; }

        [Inject]
        protected IApiScopeAppService ApiScopeAppService { get; set; }

        protected string SelectedTab = "info";

        protected List<string> AllApiResourceClaimTypes;

        protected List<string> OwnedApiResourceClaimTypes;

        protected ApiResourcePropertyDto EditingApiResourceProperty;

        protected ApiResourceSecretDto EditingApiResourceSecret;

        protected List<NameValue> SelectSecretTypeItems;

        protected List<NameValue> SelectScopeItems;

        protected IReadOnlyList<string> EditingScopes;

        protected string EditingApiResourceName;

        protected bool EditingApiResourceShowInDiscoveryDocument;

        public ApiResourcesManagement()
        {
            LocalizationResource = typeof(AbpIdentityServerResource);
            ObjectMapperContext = typeof(AbpIdentityServerBlazorModule);

            CreatePolicyName = AbpIdentityServerPermissions.ApiResource.Create;
            UpdatePolicyName = AbpIdentityServerPermissions.ApiResource.Update;
            DeletePolicyName = AbpIdentityServerPermissions.ApiResource.Delete;

            SelectSecretTypeItems = new List<NameValue>
            {
                new NameValue("Shared Secret", "SharedSecret"),
                new NameValue("X509 Thumbprint", "X509Thumbprint")
            };
            AllApiResourceClaimTypes = new List<string>();
            OwnedApiResourceClaimTypes = new List<string>();
            EditingApiResourceProperty = new ApiResourcePropertyDto();
            EditingApiResourceSecret = new ApiResourceSecretDto {Type = "Shared Secret"};
            EditingScopes = new List<string>();
        }

        protected override async Task OnInitializedAsync()
        {
            await GetAllIdentityClaimTypes();
            SelectScopeItems = (await ApiScopeAppService.GetAllListAsync()).Select(x => new NameValue(x.DisplayName, x.Name)).ToList();
            await base.OnInitializedAsync();
        }

        protected virtual async Task GetAllIdentityClaimTypes()
        {
            AllApiResourceClaimTypes = (await ClaimTypeAppService.GetListAsync()).Select(x => x.Name).ToList();
        }

        protected override async Task OpenCreateModalAsync()
        {
            OnSelectedTabChanged("info");
            OwnedApiResourceClaimTypes.Clear();
            await GetAllIdentityClaimTypes();
            await base.OpenCreateModalAsync();
        }

        protected virtual void MoveClaimType(bool toOwned, string claimType)
        {
            if (toOwned)
            {
                OwnedApiResourceClaimTypes.Add(claimType);
                AllApiResourceClaimTypes.Remove(claimType);
            }
            else
            {
                AllApiResourceClaimTypes.Add(claimType);
                OwnedApiResourceClaimTypes.Remove(claimType);
            }
        }

        protected override Task OnCreatingEntityAsync()
        {
            NewEntity.UserClaims = OwnedApiResourceClaimTypes.Select(x =>
                new ApiResourceClaimDto
                {
                    Type = x
                }).ToList();
            return base.OnCreatingEntityAsync();
        }

        protected override async Task OpenEditModalAsync(ApiResourceWithDetailsDto entity)
        {
            OnSelectedTabChanged("info");

            await GetAllIdentityClaimTypes();
            EditingApiResourceProperty = new ApiResourcePropertyDto();
            EditingApiResourceSecret = new ApiResourceSecretDto {Type = "Shared Secret"};

            EditingApiResourceName = entity.Name;
            EditingApiResourceShowInDiscoveryDocument = entity.ShowInDiscoveryDocument;

            EditValidationsRef?.ClearAll();

            await CheckUpdatePolicyAsync();

            var entityDto = await AppService.GetAsync(entity.Id);

            EditingEntityId = entity.Id;
            EditingEntity = MapToEditingEntity(entityDto);

            OwnedApiResourceClaimTypes = EditingEntity.UserClaims.Select(x => x.Type).ToList();
            AllApiResourceClaimTypes.RemoveAll(x => OwnedApiResourceClaimTypes.Contains(x));
            EditingScopes = EditingEntity.Scopes.Select(x => x.Scope).ToList();

            await InvokeAsync(() => StateHasChanged());

            EditModal.Show();
        }

        protected virtual void AddEditingSecret()
        {
            if (!EditingApiResourceSecret.Type.IsNullOrEmpty() &&
                !EditingApiResourceSecret.Value.IsNullOrEmpty())
            {
                EditingEntity.Secrets
                    .AddIfNotContains(secret =>
                            secret.Type == EditingApiResourceSecret.Type &&
                            secret.Value == EditingApiResourceSecret.Value,
                        () =>
                        {
                            var secret = ObjectMapper.Map<ApiResourceSecretDto,ApiResourceSecretDto>(EditingApiResourceSecret);
                            EditingApiResourceSecret = new ApiResourceSecretDto{Type = SelectSecretTypeItems.First().Value};
                            return secret;
                        });
            }
        }

        protected virtual void RemoveEditingSecret(ApiResourceSecretDto secret)
        {
            EditingEntity.Secrets.Remove(secret);
        }

        protected virtual void AddEditingApiResourceProperty()
        {
            if (!EditingApiResourceProperty.Key.IsNullOrEmpty() &&
                !EditingApiResourceProperty.Value.IsNullOrEmpty())
            {
                EditingEntity.Properties.AddIfNotContains(property =>
                    EditingApiResourceProperty.Key == property.Key &&
                    EditingApiResourceProperty.Value == property.Value, () =>
                {
                    var property = ObjectMapper.Map<ApiResourcePropertyDto,ApiResourcePropertyDto>(EditingApiResourceProperty);
                    EditingApiResourceProperty = new ApiResourcePropertyDto();
                    return property;
                });
            }
        }

        protected virtual void RemoveEditingApiResourceProperty(ApiResourcePropertyDto identityResourceProperty)
        {
            EditingEntity.Properties.Remove(identityResourceProperty);
        }

        protected override Task OnUpdatingEntityAsync()
        {
            EditingEntity.UserClaims = OwnedApiResourceClaimTypes
                .Select(x => new ApiResourceClaimDto {Type = x}).ToList();
            EditingEntity.Scopes = EditingScopes.Select(x => new ApiResourceScopeDto {Scope = x}).ToList();
            return base.OnUpdatingEntityAsync();
        }

        protected virtual void OnSelectedTabChanged(string name)
        {
            SelectedTab = name;
        }

        protected override ValueTask SetBreadcrumbItemsAsync()
        {
            BreadcrumbItems.Add(new BreadcrumbItem(L["Menu:IdentityServer"]));
            BreadcrumbItems.Add(new BreadcrumbItem(L["ApiResources"]));
            return ValueTask.CompletedTask;
        }

        protected override string GetDeleteConfirmationMessage(ApiResourceWithDetailsDto entity)
        {
            return L["ApiResourcesDeletionWarningMessage"];
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Volo.Abp.BlazoriseUI;
using Volo.Abp.IdentityServer.ApiResource;
using Volo.Abp.IdentityServer.ApiResource.Dtos;
using Volo.Abp.IdentityServer.ClaimType;
using Volo.Abp.IdentityServer.Client.Dtos;
using Volo.Abp.IdentityServer.IdentityResource;
using Volo.Abp.IdentityServer.IdentityResource.Dtos;
using Volo.Abp.IdentityServer.Localization;
using Volo.Abp.PermissionManagement.Blazor.Components;

namespace Volo.Abp.IdentityServer.Blazor.Pages.IdentityServer
{
    public partial class ClientManagement
    {
        protected const string PermissionProviderName = "C";

        [Inject]
        protected IIdentityResourceAppService IdentityResourceAppService { get; set; }

        [Inject]
        protected IApiResourceAppService ApiResourceAppService { get; set; }

        [Inject]
        protected IIdentityServerClaimTypeAppService ClaimTypeAppService { get; set; }

        protected PermissionManagementModal PermissionManagementModal;

        protected string ManagePermissionsPolicyName;

        protected bool HasManagePermissionsPermission { get; set; }

        protected ClientSecretDto ClientSecret;

        protected List<ClientSecretDto> ClientSecrets;

        protected List<NameValue> SelectSecretTypeItems;

        protected string CreateClientSelectedTab = "basic";

        protected string EditClientSelectedTab = "details";

        protected List<ApiResourceWithDetailsDto> AllApiResources;

        protected List<ApiResourceWithDetailsDto> AssignedApiResources;

        protected List<IdentityResourceWithDetailsDto> AllIdentityResources;

        protected List<IdentityResourceWithDetailsDto> AssignedIdentityResources;

        protected string CreateResourcesDropDownState;

        protected string EditApplicationUrlsDropDownState;

        protected string EditResourcesDropDownState;

        protected string EditAdvancedDropDownState;

        protected List<NameValue> SelectGrantTypeItems;

        protected UpdateClientModelView EditingClientModelView;

        public ClientManagement()
        {
            ObjectMapperContext = typeof(AbpIdentityServerBlazorModule);
            LocalizationResource = typeof(AbpIdentityServerResource);

            CreatePolicyName = AbpIdentityServerPermissions.Client.Create;
            UpdatePolicyName = AbpIdentityServerPermissions.Client.Update;
            DeletePolicyName = AbpIdentityServerPermissions.Client.Delete;
            ManagePermissionsPolicyName = AbpIdentityServerPermissions.Client.ManagePermissions;

            SelectSecretTypeItems = new List<NameValue>
            {
                new NameValue("Shared Secret", "SharedSecret"),
                new NameValue("X509 Thumbprint", "X509Thumbprint")
            };
            ClientSecret = new ClientSecretDto {Type = SelectSecretTypeItems.First().Value};
            ClientSecrets = new List<ClientSecretDto>();
            AllApiResources = new List<ApiResourceWithDetailsDto>();
            AssignedApiResources = new List<ApiResourceWithDetailsDto>();
            AllIdentityResources = new List<IdentityResourceWithDetailsDto>();
            AssignedIdentityResources = new List<IdentityResourceWithDetailsDto>();
            SelectGrantTypeItems = new List<NameValue>
            {
                new NameValue("implicit", "implicit"),
                new NameValue("authorization_code", "authorization_code"),
                new NameValue("hybrid", "hybrid"),
                new NameValue("client_credentials", "client_credentials"),
                new NameValue("password", "password"),
                new NameValue("custom", "custom")
            };

            EditingClientModelView = new UpdateClientModelView();
        }

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
            await GetAllResources();
        }

        protected virtual async Task GetAllResources()
        {
            AllIdentityResources = await IdentityResourceAppService.GetAllListAsync();
            AllApiResources = await ApiResourceAppService.GetAllListAsync();
        }

        protected override async Task OpenCreateModalAsync()
        {
            OnCreateClientSelectedTabChanged("basic");
            AssignedIdentityResources.Clear();
            AssignedApiResources.Clear();
            await GetAllResources();
            ClientSecret = new ClientSecretDto {Type = SelectSecretTypeItems.First().Value};
            ClientSecrets.Clear();
            await base.OpenCreateModalAsync();
        }

        protected virtual void OnCreateClientSelectedTabChanged(string name)
        {
            CreateClientSelectedTab = name;
        }

        protected virtual void OnEditClientSelectedTabChanged(string name)
        {
            EditClientSelectedTab = name;
        }

        protected virtual void MoveIdentityResources(bool toAssigned, IdentityResourceWithDetailsDto identityResource)
        {
            if (toAssigned)
            {
                AssignedIdentityResources.Add(identityResource);
                AllIdentityResources.Remove(identityResource);
            }
            else
            {
                AllIdentityResources.Add(identityResource);
                AssignedIdentityResources.Remove(identityResource);
            }
        }

        protected virtual void MoveApiResources(bool toAssigned, ApiResourceWithDetailsDto apiResource)
        {
            if (toAssigned)
            {
                AssignedApiResources.Add(apiResource);
                AllApiResources.Remove(apiResource);
            }
            else
            {
                AllApiResources.Add(apiResource);
                AssignedApiResources.Remove(apiResource);
            }
        }

        protected override Task CreateEntityAsync()
        {
            var scopes = AssignedIdentityResources.Select(x => x.Name).ToList();
            scopes.AddRange(AssignedApiResources.Select(x => x.Name).ToList());

            NewEntity.Scopes = scopes.ToArray();
            NewEntity.Secrets = ClientSecrets.ToArray();
            return base.CreateEntityAsync();
        }

        protected virtual void AddClientSecret()
        {
            if (!ClientSecret.Value.IsNullOrEmpty() && !ClientSecret.Description.IsNullOrEmpty())
            {
                ClientSecrets.AddIfNotContains(secret =>
                    secret.Value == ClientSecret.Value &&
                    secret.Type == ClientSecret.Type,() =>
                {
                    var secret = ObjectMapper.Map<ClientSecretDto,ClientSecretDto>(ClientSecret);
                    ClientSecret = new ClientSecretDto {Type = SelectSecretTypeItems.First().Value};
                    return secret;
                });
            }
        }

        protected virtual void RemoveClientSecret(ClientSecretDto clientSecret)
        {
            ClientSecrets.Remove(clientSecret);
        }

        protected override async Task OpenEditModalAsync(ClientWithDetailsDto entity)
        {
            OnEditClientSelectedTabChanged("details");

            EditValidationsRef?.ClearAll();
            await GetAllResources();

            await CheckUpdatePolicyAsync();

            var entityDto = await AppService.GetAsync(entity.Id);

            EditingEntityId = entity.Id;
            EditingEntity = MapToEditingEntity(entityDto);

            EditingClientModelView = ObjectMapper.Map<UpdateClientDto, UpdateClientModelView>(EditingEntity);
            AssignedIdentityResources = AllIdentityResources.Where(x => EditingEntity.Scopes.Contains(x.Name)).ToList();
            AssignedApiResources = AllApiResources.Where(x => EditingEntity.Scopes.Contains(x.Name)).ToList();
            AllIdentityResources.RemoveAll(AssignedIdentityResources);
            AllApiResources.RemoveAll(AssignedApiResources);

            await InvokeAsync(StateHasChanged);

            EditModal.Show();
        }

        protected virtual void ChangeCreateResourcesDropDownState()
        {
            CreateResourcesDropDownState = CreateResourcesDropDownState.IsNullOrEmpty() ? "show" : "";
        }

        protected virtual void ChangeEditApplicationUrlsDropDownState()
        {
            EditApplicationUrlsDropDownState = EditApplicationUrlsDropDownState.IsNullOrEmpty() ? "show" : "";
        }

        protected virtual void ChangeEditResourcesDropDownState()
        {
            EditResourcesDropDownState = EditResourcesDropDownState.IsNullOrEmpty() ? "show" : "";
        }

        protected virtual void ChangeEditAdvancedDropDownState()
        {
            EditAdvancedDropDownState = EditAdvancedDropDownState.IsNullOrEmpty() ? "show" : "";
        }

        protected virtual void AddEditingClientSecret()
        {
            if (!EditingClientModelView.ClientSecret.Value.IsNullOrEmpty() && !EditingClientModelView.ClientSecret.Description.IsNullOrEmpty())
            {
                EditingClientModelView.ClientSecrets.AddIfNotContains(secret =>
                    secret.Type == EditingClientModelView.ClientSecret.Type &&
                    secret.Value == EditingClientModelView.ClientSecret.Value,() =>
                {
                    var secret = ObjectMapper.Map<ClientSecretDto,ClientSecretDto>(EditingClientModelView.ClientSecret);
                    EditingClientModelView.ClientSecret = new ClientSecretDto {Type = SelectSecretTypeItems.First().Value};
                    return secret;
                });

            }
        }

        protected virtual void RemoveEditingClientSecret(ClientSecretDto clientSecret)
        {
            EditingClientModelView.ClientSecrets.Remove(clientSecret);
        }

        protected virtual void AddEditingClientClaim()
        {
            if (!EditingClientModelView.Claim.Value.IsNullOrEmpty() && !EditingClientModelView.Claim.Type.IsNullOrEmpty())
            {
                EditingClientModelView.Claims.AddIfNotContains(claim =>
                    claim.Type == EditingClientModelView.Claim.Type &&
                    claim.Value == EditingClientModelView.Claim.Value,() =>
                {
                    var claim = ObjectMapper.Map<ClientClaimDto,ClientClaimDto>(EditingClientModelView.Claim);
                    EditingClientModelView.Claim = new ClientClaimDto();
                    return claim;
                });
            }
        }

        protected virtual void RemoveEditingClientClaim(ClientClaimDto clientClaim)
        {
            EditingClientModelView.Claims.Remove(clientClaim);
        }

        protected virtual void AddEditingRedirectUri()
        {
            if (!EditingClientModelView.CallbackUri.IsNullOrEmpty())
            {
                EditingClientModelView.RedirectUris.AddIfNotContains(EditingClientModelView.CallbackUri);
            }
        }

        protected virtual void RemoveEditingRedirectUri(string index)
        {
            EditingClientModelView.RedirectUris.Remove(index);
        }

        protected virtual void AddEditingGrantType()
        {
            if (EditingClientModelView.GrantType.IsNullOrEmpty())
            {
                return;
            }

            if (EditingClientModelView.GrantType == "custom")
            {
                if (!EditingClientModelView.CustomGrantType.IsNullOrEmpty())
                {
                    EditingClientModelView.AllowedGrantTypes.AddIfNotContains(EditingClientModelView.CustomGrantType);
                }
            }
            else
            {
                EditingClientModelView.AllowedGrantTypes.AddIfNotContains(EditingClientModelView.GrantType);
            }
        }

        protected virtual void RemoveEditingGrantType(string grantType)
        {
            EditingClientModelView.AllowedGrantTypes.Remove(grantType);
        }

        protected virtual void AddEditingRestriction()
        {
            if (!EditingClientModelView.Restriction.IsNullOrEmpty())
            {
                EditingClientModelView.IdentityProviderRestrictions.AddIfNotContains(EditingClientModelView
                    .Restriction);
            }
        }

        protected virtual void RemoveEditingRestriction(string restriction)
        {
            EditingClientModelView.IdentityProviderRestrictions.Remove(restriction);
        }

        protected virtual void AddEditingSignOutUrl()
        {
            if (!EditingClientModelView.PostLogoutRedirectUri.IsNullOrEmpty())
            {
                EditingClientModelView.PostLogoutRedirectUris.AddIfNotContains(EditingClientModelView
                    .PostLogoutRedirectUri);
            }
        }

        protected virtual void RemoveEditingSignOutUrl(string signOutUrl)
        {
            EditingClientModelView.PostLogoutRedirectUris.Remove(signOutUrl);
        }

        protected virtual void AddEditingCorsOrigin()
        {
            if (!EditingClientModelView.CorsOrigin.IsNullOrEmpty())
            {
                EditingClientModelView.AllowedCorsOrigins.AddIfNotContains(EditingClientModelView.CorsOrigin);
            }
        }

        protected virtual void RemoveEditingCorsOrigin(string corsOrigin)
        {
            EditingClientModelView.AllowedCorsOrigins.Remove(corsOrigin);
        }

        protected virtual void AddEditingClientProperty()
        {
            if (!EditingClientModelView.Property.Key.IsNullOrEmpty() &&
                !EditingClientModelView.Property.Value.IsNullOrEmpty())
            {
                EditingClientModelView.Properties.AddIfNotContains(clientProperty =>
                        clientProperty.Key == EditingClientModelView.Property.Key &&
                        clientProperty.Value == EditingClientModelView.Property.Value,
                    () =>
                    {
                        var property = ObjectMapper.Map<ClientPropertyDto,ClientPropertyDto>(EditingClientModelView.Property);
                        EditingClientModelView.Property = new ClientPropertyDto();
                        return property;
                    });
            }
        }

        protected virtual void RemoveEditingClientProperty(ClientPropertyDto clientProperty)
        {
            EditingClientModelView.Properties.Remove(clientProperty);
        }

        protected override Task OnUpdatingEntityAsync()
        {
            var scopes = AssignedIdentityResources.Select(x => x.Name).ToList();
            scopes.AddRange(AssignedApiResources.Select(x => x.Name).ToList());

            EditingEntity.Scopes = scopes.ToArray();
            EditingEntity = ObjectMapper.Map(EditingClientModelView, EditingEntity);

            return Task.CompletedTask;
        }

        protected override string GetDeleteConfirmationMessage(ClientWithDetailsDto entity)
        {
            return string.Format(L["ClientsDeletionWarningMessage"]);
        }

        protected override ValueTask SetBreadcrumbItemsAsync()
        {
            BreadcrumbItems.Add(new BreadcrumbItem(L["Menu:IdentityServer"]));
            BreadcrumbItems.Add(new BreadcrumbItem(L["Clients"]));
            return ValueTask.CompletedTask;
        }
    }

    public class UpdateClientModelView
    {
        public UpdateClientModelView()
        {
            RedirectUris = new List<string>();
            ClientSecret = new ClientSecretDto
            {
                Type = "SharedSecret"
            };
            ClientSecrets = new List<ClientSecretDto>();
            Claim = new ClientClaimDto();
            Claims = new List<ClientClaimDto>();
            AllowedGrantTypes = new List<string>();
            IdentityProviderRestrictions = new List<string>();
            PostLogoutRedirectUris = new List<string>();
            AllowedCorsOrigins = new List<string>();
            Properties = new List<ClientPropertyDto>();
            Property = new ClientPropertyDto();
        }

        public string CallbackUri { get; set; }

        public List<string> RedirectUris { get; set; }

        public ClientSecretDto ClientSecret { get; set; }

        public List<ClientSecretDto> ClientSecrets { get; set; }

        public ClientClaimDto Claim { get; set; }

        public List<ClientClaimDto> Claims { get; set; }

        public string GrantType { get; set; }

        public string CustomGrantType { get; set; }

        public List<string> AllowedGrantTypes { get; set; }

        public string Restriction { get; set; }

        public List<string> IdentityProviderRestrictions { get; set; }

        public string PostLogoutRedirectUri { get; set; }

        public List<string> PostLogoutRedirectUris { get; set; }

        public string CorsOrigin { get; set; }

        public List<string> AllowedCorsOrigins { get; set; }

        public ClientPropertyDto Property { get; set; }

        public List<ClientPropertyDto> Properties { get; set; }
    }
}

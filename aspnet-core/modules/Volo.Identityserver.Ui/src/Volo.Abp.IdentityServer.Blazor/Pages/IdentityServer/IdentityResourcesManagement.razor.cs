﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Volo.Abp.BlazoriseUI;
using Volo.Abp.IdentityServer.ClaimType;
using Volo.Abp.IdentityServer.IdentityResource.Dtos;
using Volo.Abp.IdentityServer.Localization;

namespace Volo.Abp.IdentityServer.Blazor.Pages.IdentityServer
{
    public partial class IdentityResourcesManagement
    {
        [Inject]
        protected IIdentityServerClaimTypeAppService ClaimTypeAppService { get; set; }

        protected List<string> AllIdentityClaimTypes;

        protected List<string> OwnedIdentityClaimTypes;

        protected string SelectedTab = "info";

        protected IdentityResourcePropertyDto EditingIdentityResourceProperty;

        public IdentityResourcesManagement()
        {
            LocalizationResource = typeof(AbpIdentityServerResource);
            ObjectMapperContext = typeof(AbpIdentityServerBlazorModule);

            CreatePolicyName = AbpIdentityServerPermissions.IdentityResource.Create;
            UpdatePolicyName = AbpIdentityServerPermissions.IdentityResource.Update;
            DeletePolicyName = AbpIdentityServerPermissions.IdentityResource.Delete;

            AllIdentityClaimTypes = new List<string>();
            OwnedIdentityClaimTypes = new List<string>();
            EditingIdentityResourceProperty = new IdentityResourcePropertyDto();
        }

        protected override async Task OnInitializedAsync()
        {
            await GetAllIdentityClaimTypes();
            await base.OnInitializedAsync();
        }

        protected virtual async Task GetAllIdentityClaimTypes()
        {
            AllIdentityClaimTypes = (await ClaimTypeAppService.GetListAsync()).Select(x=>x.Name).ToList();
        }

        protected override async Task OpenCreateModalAsync()
        {
            OnSelectedTabChanged("info");

            OwnedIdentityClaimTypes.Clear();
            await GetAllIdentityClaimTypes();

            await base.OpenCreateModalAsync();

            NewEntity.Enabled = true;
            NewEntity.ShowInDiscoveryDocument = true;
        }

        protected virtual void MoveClaimType(bool toOwned, string claimType)
        {
            if (toOwned)
            {
                OwnedIdentityClaimTypes.Add(claimType);
                AllIdentityClaimTypes.Remove(claimType);
            }
            else
            {
                AllIdentityClaimTypes.Add(claimType);
                OwnedIdentityClaimTypes.Remove(claimType);
            }
        }

        protected override Task OnCreatingEntityAsync()
        {
            NewEntity.UserClaims = OwnedIdentityClaimTypes.Select(x =>
                new IdentityResourceClaimDto
            {
                Type = x
            }).ToList();
            NewEntity.Properties = new List<IdentityResourcePropertyDto>();
            return base.OnCreatingEntityAsync();
        }

        protected virtual void OnSelectedTabChanged(string name)
        {
            SelectedTab = name;
        }

        protected virtual async Task CreateStandardResources()
        {
            if (await Message.Confirm(L["CreateStandardIdentityResourcesWarningMessage"]))
            {
                await AppService.CreateStandardResourcesAsync();
                await GetEntitiesAsync();
            }
        }

        protected virtual void AddEditingIdentityResourceProperty()
        {
            if (!EditingIdentityResourceProperty.Key.IsNullOrEmpty() &&
                !EditingIdentityResourceProperty.Value.IsNullOrEmpty())
            {
                EditingEntity.Properties.AddIfNotContains(property =>
                    EditingIdentityResourceProperty.Key == property.Key &&
                    EditingIdentityResourceProperty.Value == property.Value, () =>
                {
                    var property = ObjectMapper.Map<IdentityResourcePropertyDto,IdentityResourcePropertyDto>(EditingIdentityResourceProperty);
                    EditingIdentityResourceProperty = new IdentityResourcePropertyDto();
                    return property;
                });
            }
        }

        protected virtual void RemoveEditingIdentityResourceProperty(IdentityResourcePropertyDto identityResourceProperty)
        {
            EditingEntity.Properties.Remove(identityResourceProperty);
        }

        protected override async Task OpenEditModalAsync(IdentityResourceWithDetailsDto entity)
        {

            OnSelectedTabChanged("info");

            await GetAllIdentityClaimTypes();
            EditingIdentityResourceProperty = new IdentityResourcePropertyDto();

            EditValidationsRef?.ClearAll();

            await CheckUpdatePolicyAsync();

            var entityDto = await AppService.GetAsync(entity.Id);

            EditingEntityId = entity.Id;
            EditingEntity = MapToEditingEntity(entityDto);

            OwnedIdentityClaimTypes = EditingEntity.UserClaims.Select(x => x.Type).ToList();
            AllIdentityClaimTypes.RemoveAll(x => OwnedIdentityClaimTypes.Contains(x));

            await InvokeAsync(() => StateHasChanged());

            EditModal.Show();
        }

        protected override Task OnUpdatingEntityAsync()
        {
            EditingEntity.UserClaims = OwnedIdentityClaimTypes
                .Select(x=> new IdentityResourceClaimDto{Type = x}).ToList();
            return base.OnUpdatingEntityAsync();
        }

        protected override ValueTask SetBreadcrumbItemsAsync()
        {
            BreadcrumbItems.Add(new BreadcrumbItem(L["Menu:IdentityServer"]));
            BreadcrumbItems.Add(new BreadcrumbItem(L["IdentityResources"]));
            return ValueTask.CompletedTask;
        }

        protected override string GetDeleteConfirmationMessage(IdentityResourceWithDetailsDto entity)
        {
            return L["IdentityResourcesDeletionWarningMessage"];
        }
    }
}

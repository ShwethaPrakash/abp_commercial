﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Volo.Abp.AuditLogging.Blazor.Components;
using Volo.Abp.BlazoriseUI;
using Volo.Abp.IdentityServer.ApiScope.Dtos;
using Volo.Abp.IdentityServer.ClaimType;
using Volo.Abp.IdentityServer.Localization;


namespace Volo.Abp.IdentityServer.Blazor.Pages.IdentityServer
{
    public partial class ApiScopesManagement
    {
        protected const string EntityTypeFullName = "Volo.Abp.IdentityServer.ApiScopes.ApiScope";

        [Inject]
        protected IIdentityServerClaimTypeAppService ClaimTypeAppService { get; set; }

        protected List<string> AllApiScopesClaimTypes;

        protected List<string> OwnedApiScopesClaimTypes;

        protected string SelectedTab = "info";

        protected ApiScopePropertyDto EditingApiScopeProperty;

        protected EntityChangeHistoryModal EntityChangeHistoryModal;

        public ApiScopesManagement()
        {
            LocalizationResource = typeof(AbpIdentityServerResource);
            ObjectMapperContext = typeof(AbpIdentityServerBlazorModule);

            CreatePolicyName = AbpIdentityServerPermissions.ApiScope.Create;
            UpdatePolicyName = AbpIdentityServerPermissions.ApiScope.Update;
            DeletePolicyName = AbpIdentityServerPermissions.ApiScope.Delete;

            AllApiScopesClaimTypes = new List<string>();
            OwnedApiScopesClaimTypes = new List<string>();
            EditingApiScopeProperty = new ApiScopePropertyDto();
        }

        protected override async Task OnInitializedAsync()
        {
            await GetAllApiScopesClaimTypes();
            await base.OnInitializedAsync();
        }

        protected virtual async Task GetAllApiScopesClaimTypes()
        {
            AllApiScopesClaimTypes = (await ClaimTypeAppService.GetListAsync()).Select(x=>x.Name).ToList();
        }

        protected override async Task OpenCreateModalAsync()
        {
            OnSelectedTabChanged("info");
            OwnedApiScopesClaimTypes.Clear();
            await GetAllApiScopesClaimTypes();
            await base.OpenCreateModalAsync();
        }

        protected virtual void MoveClaimType(bool toOwned, string claimType)
        {
            if (toOwned)
            {
                OwnedApiScopesClaimTypes.Add(claimType);
                AllApiScopesClaimTypes.Remove(claimType);
            }
            else
            {
                AllApiScopesClaimTypes.Add(claimType);
                OwnedApiScopesClaimTypes.Remove(claimType);
            }
        }

        protected override Task OnCreatingEntityAsync()
        {
            NewEntity.UserClaims = OwnedApiScopesClaimTypes.Select(x =>
                new ApiScopeClaimDto
                {
                    Type = x
                }).ToList();
            NewEntity.Properties = new List<ApiScopePropertyDto>();
            return base.OnCreatingEntityAsync();
        }

        protected override async Task OpenEditModalAsync(ApiScopeWithDetailsDto entity)
        {
            OnSelectedTabChanged("info");

            await GetAllApiScopesClaimTypes();
            EditingApiScopeProperty = new ApiScopePropertyDto();

            EditValidationsRef?.ClearAll();

            await CheckUpdatePolicyAsync();

            var entityDto = await AppService.GetAsync(entity.Id);

            EditingEntityId = entity.Id;
            EditingEntity = MapToEditingEntity(entityDto);

            OwnedApiScopesClaimTypes = EditingEntity.UserClaims.Select(x => x.Type).ToList();
            AllApiScopesClaimTypes.RemoveAll(x => OwnedApiScopesClaimTypes.Contains(x));

            await InvokeAsync(() => StateHasChanged());

            EditModal.Show();
        }

        protected virtual void AddEditingApiScopeProperty()
        {
            if (!EditingApiScopeProperty.Key.IsNullOrEmpty() &&
                !EditingApiScopeProperty.Value.IsNullOrEmpty())
            {
                EditingEntity.Properties.AddIfNotContains(property =>
                    EditingApiScopeProperty.Key == property.Key &&
                    EditingApiScopeProperty.Value == property.Value, () =>
                {
                    var property = ObjectMapper.Map<ApiScopePropertyDto,ApiScopePropertyDto>(EditingApiScopeProperty);
                    EditingApiScopeProperty = new ApiScopePropertyDto();
                    return property;
                });
            }
        }

        protected virtual void RemoveEditingApiScopeProperty(ApiScopePropertyDto apiScopeProperty)
        {
            EditingEntity.Properties.Remove(apiScopeProperty);
        }

        protected override Task OnUpdatingEntityAsync()
        {
            EditingEntity.UserClaims = OwnedApiScopesClaimTypes
                .Select(x => new ApiScopeClaimDto {Type = x}).ToList();
            return base.OnUpdatingEntityAsync();
        }

        protected virtual void OnSelectedTabChanged(string name)
        {
            SelectedTab = name;
        }

        protected override ValueTask SetBreadcrumbItemsAsync()
        {
            BreadcrumbItems.Add(new BreadcrumbItem(L["Menu:IdentityServer"]));
            BreadcrumbItems.Add(new BreadcrumbItem(L["ApiScopes"]));
            return ValueTask.CompletedTask;
        }

        protected override string GetDeleteConfirmationMessage(ApiScopeWithDetailsDto entity)
        {
            return L["ApiScopesDeletionWarningMessage"];
        }
    }
}

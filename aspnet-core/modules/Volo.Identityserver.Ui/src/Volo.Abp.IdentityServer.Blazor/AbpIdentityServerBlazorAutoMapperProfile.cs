﻿using System.Linq;
using AutoMapper;
using Volo.Abp.AutoMapper;
using Volo.Abp.IdentityServer.ApiResource.Dtos;
using Volo.Abp.IdentityServer.ApiScope.Dtos;
using Volo.Abp.IdentityServer.Blazor.Pages.IdentityServer;
using Volo.Abp.IdentityServer.Client.Dtos;
using Volo.Abp.IdentityServer.IdentityResource.Dtos;

namespace Volo.Abp.IdentityServer.Blazor
{
    public class AbpIdentityServerBlazorAutoMapperProfile : Profile
    {
        public AbpIdentityServerBlazorAutoMapperProfile()
        {
            CreateMap<ClientWithDetailsDto, UpdateClientDto>()
                .MapExtraProperties()
                .ForMember(destination => destination.AllowedGrantTypes,
                    m => m.MapFrom(source => source.AllowedGrantTypes.Select(x => x.GrantType)))
                .ForMember(destination => destination.IdentityProviderRestrictions,
                    m => m.MapFrom(source => source.IdentityProviderRestrictions.Select(x => x.Provider)))
                .ForMember(destination => destination.Scopes,
                    m => m.MapFrom(source => source.AllowedScopes.Select(x => x.Scope)))
                .ForMember(destination => destination.AllowedCorsOrigins,
                    m => m.MapFrom(source => source.AllowedCorsOrigins.Select(x => x.Origin)))
                .ForMember(destination => destination.RedirectUris,
                    m => m.MapFrom(source => source.RedirectUris.Select(x => x.RedirectUri)))
                .ForMember(destination => destination.PostLogoutRedirectUris,
                    m => m.MapFrom(source => source.PostLogoutRedirectUris.Select(x => x.PostLogoutRedirectUri)));

            CreateMap<UpdateClientDto, UpdateClientModelView>(MemberList.None);
            CreateMap<UpdateClientModelView, UpdateClientDto>(MemberList.None);

            CreateMap<IdentityResourceWithDetailsDto, UpdateIdentityResourceDto>()
                .MapExtraProperties();

            CreateMap<ApiResourceWithDetailsDto, UpdateApiResourceDto>()
                .MapExtraProperties();

            CreateMap<ApiScopeWithDetailsDto, UpdateApiScopeDto>()
                .MapExtraProperties();

            CreateMap<ClientPropertyDto, ClientPropertyDto>();
            CreateMap<ClientClaimDto, ClientClaimDto>();
            CreateMap<ClientSecretDto, ClientSecretDto>();

            CreateMap<IdentityResourcePropertyDto, IdentityResourcePropertyDto>();

            CreateMap<ApiResourceSecretDto, ApiResourceSecretDto>();
            CreateMap<ApiResourcePropertyDto, ApiResourcePropertyDto>();
            CreateMap<ApiResourceScopeDto, ApiResourceScopeDto>();

            CreateMap<ApiScopePropertyDto, ApiScopePropertyDto>();

        }
    }
}

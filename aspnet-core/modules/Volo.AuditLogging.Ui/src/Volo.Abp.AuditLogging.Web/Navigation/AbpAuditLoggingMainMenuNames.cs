﻿namespace Volo.Abp.AuditLogging.Web.Navigation
{
    public class AbpAuditLoggingMainMenuNames
    {
        public const string GroupName = "AbpAuditLogging";
    }
}

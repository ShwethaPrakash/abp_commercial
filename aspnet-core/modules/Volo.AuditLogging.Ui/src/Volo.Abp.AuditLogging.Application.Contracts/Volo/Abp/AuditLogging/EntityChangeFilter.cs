﻿namespace Volo.Abp.AuditLogging
{
    public class EntityChangeFilter
    {
        public string EntityId { get; set; }

        public string EntityTypeFullName { get; set; }
    }
}
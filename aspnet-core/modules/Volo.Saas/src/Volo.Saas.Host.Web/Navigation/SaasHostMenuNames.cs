﻿namespace Volo.Saas.Host.Navigation
{
    public class SaasHostMenuNames
    {
        public const string GroupName = "SaasHost";

        public const string Tenants = GroupName + ".Tenants";

        public const string Editions = GroupName + ".Editions";
    }
}

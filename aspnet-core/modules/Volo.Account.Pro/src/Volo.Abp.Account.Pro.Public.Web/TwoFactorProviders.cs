﻿namespace Volo.Abp.Account.Public.Web
{
    //TODO: Is there any constans inside the Identity library?
    public static class TwoFactorProviders
    {
        public const string Email = "Email";
        public const string Phone = "Phone";
        public const string GoogleAuthenticator = "GoogleAuthenticator"; //TODO: Not implemented yet
    }
}

﻿namespace Volo.Payment.Iyzico
{
    public class IyzicoConsts
    {
        /// <summary>
        /// Value: "iyzico"
        /// </summary>
        public const string GatewayName = "iyzico";

        /// <summary>
        /// Value: "/Payment/Iyzico/PrePayment"
        /// </summary>
        public const string PrePaymentUrl = "/Payment/Iyzico/PrePayment";

        /// <summary>
        /// Value: "/Payment/Iyzico/PostPayment"
        /// </summary>
        public const string PostPaymentUrl = "/Payment/Iyzico/PostPayment";
    }
}

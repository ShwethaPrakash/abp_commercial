﻿using Volo.Abp.Settings;

namespace Volo.Payment
{
    public class PaymentSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from PaymentSettings class.
             */
        }
    }
}
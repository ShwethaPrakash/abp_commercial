﻿namespace Volo.Payment.Payu
{
    public class PayuPaymentRequestProductExtraParameterConfiguration : IPaymentRequestProductExtraParameterConfiguration
    {
        public int? VatRate { get; set; }
    }
}
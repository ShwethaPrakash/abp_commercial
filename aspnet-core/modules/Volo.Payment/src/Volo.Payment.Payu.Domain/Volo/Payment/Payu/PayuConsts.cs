﻿namespace Volo.Payment.Payu
{
    public class PayuConsts
    {
        /// <summary>
        /// Value: "payu"
        /// </summary>
        public const string GatewayName = "payu";

        /// <summary>
        /// Value: "/Payment/Payu/PrePayment"
        /// </summary>
        public const string PrePaymentUrl = "/Payment/Payu/PrePayment";

        /// <summary>
        /// Value: "/Payment/Payu/PostPayment"
        /// </summary>
        public const string PostPaymentUrl = "/Payment/Payu/PostPayment";
    }
}

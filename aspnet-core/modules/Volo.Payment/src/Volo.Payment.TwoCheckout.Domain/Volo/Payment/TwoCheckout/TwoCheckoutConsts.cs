﻿namespace Volo.Payment.TwoCheckout
{
    public class TwoCheckoutConsts
    {
        /// <summary>
        /// Value: "two-checkout""
        /// </summary>
        public const string GatewayName = "two-checkout";

        /// <summary>
        /// Value: "/Payment/TwoCheckout/PrePayment"
        /// </summary>
        public const string PrePaymentUrl = "/Payment/TwoCheckout/PrePayment";

        /// <summary>
        /// Value: "/Payment/TwoCheckout/PostPayment"
        /// </summary>
        public const string PostPaymentUrl = "/Payment/TwoCheckout/PostPayment";
    }
}

﻿namespace Volo.Payment.Stripe
{
    public class StripeConsts
    {
        /// <summary>
        /// Value: "stripe"
        /// </summary>
        public const string GatewayName = "stripe";

        /// <summary>
        /// Value: "/Payment/Stripe/PrePayment"
        /// </summary>
        public const string PrePaymentUrl = "/Payment/Stripe/PrePayment";

        /// <summary>
        /// Value: "/Payment/Stripe/PostPayment"
        /// </summary>
        public const string PostPaymentUrl = "/Payment/Stripe/PostPayment";
    }
}

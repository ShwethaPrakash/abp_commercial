﻿using Volo.Abp.Localization;

namespace Volo.Payment.Localization
{
    [LocalizationResourceName("Payment")]
    public class PaymentResource
    {
        
    }
}

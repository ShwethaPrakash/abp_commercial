﻿namespace Volo.Payment
{
    public static class PaymentDbProperties
    {
        public static string DefaultDbTablePrefix { get; } = "Pay";

        public static string DefaultDbSchema { get; } = null;
    }
}

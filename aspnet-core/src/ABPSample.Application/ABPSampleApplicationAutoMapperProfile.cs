using AutoMapper;
using ABPSample.Users;
using Volo.Abp.AutoMapper;

namespace ABPSample
{
    public class ABPSampleApplicationAutoMapperProfile : Profile
    {
        public ABPSampleApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */

            CreateMap<AppUser, AppUserDto>().Ignore(x => x.ExtraProperties);
        }
    }
}
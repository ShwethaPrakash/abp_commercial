﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace ABPSample.EntityFrameworkCore
{
    [DependsOn(
        typeof(ABPSampleEntityFrameworkCoreModule)
    )]
    public class ABPSampleEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<ABPSampleMigrationsDbContext>();
        }
    }
}
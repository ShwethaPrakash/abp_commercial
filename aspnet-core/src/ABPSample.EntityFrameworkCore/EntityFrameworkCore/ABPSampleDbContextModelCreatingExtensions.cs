using Microsoft.EntityFrameworkCore;
using Volo.Abp;

namespace ABPSample.EntityFrameworkCore
{
    public static class ABPSampleDbContextModelCreatingExtensions
    {
        public static void ConfigureABPSample(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */

            //builder.Entity<YourEntity>(b =>
            //{
            //    b.ToTable(ABPSampleConsts.DbTablePrefix + "YourEntities", ABPSampleConsts.DbSchema);
            //    b.ConfigureByConvention(); //auto configure for the base class props
            //    //...
            //});
        }
    }
}
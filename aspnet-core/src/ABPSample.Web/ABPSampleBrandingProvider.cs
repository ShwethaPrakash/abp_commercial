﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace ABPSample.Web
{
    [Dependency(ReplaceServices = true)]
    public class ABPSampleBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "ABPSample";
    }
}

﻿namespace ABPSample.Web.Menus
{
    public class ABPSampleMenus
    {
        private const string Prefix = "ABPSample";

        public const string Home = Prefix + ".Home";

        public const string HostDashboard = Prefix + ".HostDashboard";

        public const string TenantDashboard = Prefix + ".TenantDashboard";
    }
}

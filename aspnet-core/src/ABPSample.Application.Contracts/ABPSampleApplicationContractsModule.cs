using Volo.Abp.Account;
using Volo.Abp.AuditLogging;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Identity;
using Volo.Abp.IdentityServer;
using Volo.Abp.LanguageManagement;
using Volo.Abp.LeptonTheme.Management;
using Volo.Abp.Modularity;
using Volo.Abp.ObjectExtending;
using Volo.Abp.PermissionManagement;
using Volo.Abp.TextTemplateManagement;
using Volo.Saas.Host;
using Volo.Saas.Tenant;
using Volo.Payment;
using Volo.Chat;
using Volo.FileManagement;

namespace ABPSample
{
    [DependsOn(
        typeof(ABPSampleDomainSharedModule),
        typeof(AbpFeatureManagementApplicationContractsModule),
        typeof(AbpIdentityApplicationContractsModule),
        typeof(AbpPermissionManagementApplicationContractsModule),
        typeof(SaasHostApplicationContractsModule),
        typeof(AbpAuditLoggingApplicationContractsModule),
        typeof(AbpIdentityServerApplicationContractsModule),
        typeof(AbpAccountPublicApplicationContractsModule),
        typeof(AbpAccountAdminApplicationContractsModule),
        typeof(LanguageManagementApplicationContractsModule),
        typeof(LeptonThemeManagementApplicationContractsModule),
        typeof(TextTemplateManagementApplicationContractsModule)
    )]
    [DependsOn(typeof(AbpAccountSharedApplicationContractsModule))]
    [DependsOn(typeof(SaasTenantApplicationContractsModule))]
    [DependsOn(typeof(AbpPaymentApplicationContractsModule))]
    [DependsOn(typeof(ChatApplicationContractsModule))]
    [DependsOn(typeof(FileManagementApplicationContractsModule))]
    public class ABPSampleApplicationContractsModule : AbpModule
    {

    }
}

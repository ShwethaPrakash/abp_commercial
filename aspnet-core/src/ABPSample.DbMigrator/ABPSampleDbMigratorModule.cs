﻿using ABPSample.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace ABPSample.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(ABPSampleEntityFrameworkCoreDbMigrationsModule),
        typeof(ABPSampleApplicationContractsModule)
    )]
    public class ABPSampleDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options =>
            {
                options.IsJobExecutionEnabled = false;
            });
        }
    }
}
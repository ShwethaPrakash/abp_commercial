﻿using Volo.Abp.Localization;

namespace ABPSample.Localization
{
    [LocalizationResourceName("ABPSample")]
    public class ABPSampleResource
    {

    }
}
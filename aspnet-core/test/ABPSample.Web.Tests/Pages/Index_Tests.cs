﻿using System.Threading.Tasks;
using Shouldly;
using Xunit;

namespace ABPSample.Pages
{
    public class Index_Tests : ABPSampleWebTestBase
    {
        [Fact]
        public async Task Welcome_Page()
        {
            var response = await GetResponseAsStringAsync("/");
            response.ShouldNotBeNull();
        }
    }
}

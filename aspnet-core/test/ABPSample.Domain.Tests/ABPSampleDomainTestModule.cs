﻿using ABPSample.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace ABPSample
{
    [DependsOn(
        typeof(ABPSampleEntityFrameworkCoreTestModule)
        )]
    public class ABPSampleDomainTestModule : AbpModule
    {

    }
}
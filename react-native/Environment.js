const ENV = {
  dev: {
        apiUrl: 'http://localhost:44379',
    oAuthConfig: {
        issuer: 'http://localhost:44379',
        clientId: 'ABPSample_App',
      clientSecret: '1q2w3e*',
        scope: 'offline_access ABPSample',
    },
    localization: {
        defaultResourceName: 'ABPSample',
    },
  },
  prod: {
      apiUrl: 'http://localhost:44379',
    oAuthConfig: {
        issuer: 'http://localhost:44379',
        clientId: 'ABPSample_App',
      clientSecret: '1q2w3e*',
        scope: 'offline_access ABPSample',
    },
    localization: {
        defaultResourceName: 'ABPSample',
    },
  },
};

export const getEnvVars = () => {
  // eslint-disable-next-line no-undef
  return __DEV__ ? ENV.dev : ENV.prod;
};
